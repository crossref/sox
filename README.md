# sox

## Note (2021-02-01)

This used to be a heavily modified version of a bash script originally found at:

https://github.com/PHLAK/Soxy

I have since discovered `autossh` and have been using that instead of the sox shell script. Assuming you have defined `bastion` in your ~/.ssh/config`, you can basically accomplish the same thing as the old scripts with:

```
autossh -M 0 -D 8123 -f -q -N bastion
```

### To start the socks proxy automatically on linux:

- make sure `autossh` is installed
- define `bastion` in your `~/.ssh/config`
- copy the included example `.service` file to `/etc/systemd/system/autossh-datacenter-socks.service`
- replace the `<your-user-name>` in the service file with your own USER name.
- add the service with `systemctl daemon-reload`
- start the service with `systemctl start autossh-datacenter-socks.service`
- set the service to activate at boot with `systemctl enable `autossh-datacenter-socks.service`

### To start the socks proxy automatically on Macos:

- TODO



Repo icon CC-BY
Irwin Lowe
via the Noun Project


## Tips

Note this SOCKS proxy is particulaerly useful when combined with a browser extension like [Foxyproxy](https://addons.mozilla.org/en-US/firefox/addon/foxyproxy-standard/) which allows you to define URL patterns that will always trigger the use of the socks proxy for connections.