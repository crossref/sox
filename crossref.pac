
// Proxy PAC File
// - Used to redirect certain addresses to the server through the SOCKS ssh port (8123 for this file), i.e. 
// - To install on OS X/MacOS, go to "Settings->Network->Advanced->Proxies->Automatic Proxy Configuration"

function FindProxyForURL(url, host) {
  // Setup a SOCKS proxy on port 8123.
  proxy = "SOCKS5 localhost:8123";
  // Setup proxy filters.
  // - Use `host` for IP addresses and domain names.
  // - Use `url` for more control over the entire URL (i.e. sub paths).
  // if (isPlainHostName(host)) return "DIRECT";
  
  if (shExpMatch(host, "172.20.1.*") ||  // match IP address
      shExpMatch(host, "logman*") ||
      shExpMatch(host, "prom*")) {  // match `server1`, `server23`, etc.
    return proxy;
  }

  // Route everything else directly!
  return "DIRECT";
}

